import { MiddlewareFn } from "type-graphql";
import { verify } from "jsonwebtoken"; // este metodo hara un decode de nuestro token y va a chequear que el usuario sea el usuario logeado
import { Response, Request } from "express";
import { environment } from "../config/environment";
// import { User } from "../entity/user.entity";

// es una buena practica agregar una I en el nombre de la Interface
export interface IContext {
  req: Request;
  res: Response;
  payload: { userId: string };
  // user: User // creamos propiedad user con la entidad User y podriamos devolver el usuario completo
}

// MiddlewareFn va a recibir lo que es IContext
export const isAuth: MiddlewareFn<IContext> = ({ context }, next) => {
  try {
    // lo primero que haremos es agarrar nuestro token
    const bearerToken = context.req.headers["authorization"]; // tomamos nuestro token de authorization

    if (!bearerToken) {
      throw new Error("Unauthorized");
    }

    // debemos agarrar el token en si mismo: en el header vamos a recibir un string como "Bearer jewhdkjehkjhekdl"
    // pero al verify solo debemos pasarle lo que sigue despues del Bedarer. Lo obtendremos con js
    // al hacer un split con " " nos queda un ["Bearer","jewhdkjehkjhekdl"]
    const jwt = bearerToken.split(" ")[1]; // split divide el string de acuerdo al caracter que le pasemos
    const payload = verify(jwt, environment.JWT_SECRET); // para poder verificar el token le debemos pasar la palabra secreta
    // ahora el payload debemos agregarlo al contexto para que lo tengamos disponible para todos los resolvers
    context.payload = payload as any; // nuestro payload es el userId, con nuestro userId vamos a poder obtener el usuario logeado
  } catch (e) {
    throw new Error(e);
  }

  return next(); // este next es de express y va a continuar con la ejecucion del programa. Si no se lo pasamos se queda en lo de arriba
};
