import { IsEmail, Length } from "class-validator";
import {
  Arg,
  Field,
  InputType,
  Mutation,
  ObjectType,
  Resolver,
} from "type-graphql";
import { getRepository, Repository } from "typeorm";
import { User } from "../entity/user.entity";
import { hash, compareSync } from "bcryptjs"; // para encriptar la password. Recuerda que es asincrono (tambien hay un asyncHash pero ese no lo usaremos). Tenemos tambien compare y compareSync para desencriptar la password y validar contraseña ingresada por el usuario
import { sign } from "jsonwebtoken";
import { environment } from "../config/environment";

@InputType()
class UserInput {
  @Field()
  @Length(3, 64)
  fullName!: string;

  @Field()
  @IsEmail() // esto valida que el usuario no nos mande algo que no sea un email
  email!: string;

  @Field()
  @Length(8, 254)
  password!: string;
}

// creamos el input para el login
@InputType()
class LoginInput {
  @Field()
  @IsEmail() // validamos que sea email
  email!: string;

  @Field()
  password!: string;
}

@ObjectType() // esto ya no es un input sino una respuesta
class LoginResponse {
  @Field()
  userId!: number;

  @Field()
  jwt!: string; // jwt = Json Web Token
}

@Resolver()
export class AuthResolver {
  // inicializamos un repositorio que vamos a necesitar
  userRepository: Repository<User>;

  constructor() {
    this.userRepository = getRepository(User);
  }

  @Mutation(() => User) // importada desde type-graphql. Nos devuelve la entidad User
  async register(
    @Arg("input", () => UserInput) input: UserInput
  ): Promise<User | undefined> {
    try {
      // desestructuramos del input la informacion que nos mando el usuario
      const { fullName, email, password } = input;
      // si la BD encuentra un usuario que ya existe, tenemos que arrojar un error (no podemos volver a crear un mismo usuario)
      const userExists = await this.userRepository.findOne({
        where: { email },
      }); // buscamos un usuario por el email que nos acaba de mandar

      if (userExists) {
        const error = new Error();
        error.message = "Email is not available";
        throw error;
      }

      // recuerda que hash es asincrono
      const hashedPassword = await hash(password, 10); // el salt es 10
      // nos queda crear el usuario
      const newUser = await this.userRepository.insert({
        fullName,
        email,
        password: hashedPassword,
      }); // recuerda que tambien lo puedes hacer con create o con save

      return this.userRepository.findOne(newUser.identifiers[0].id); // recuerda que findOne nos devuelve un objeto que tiene un identifiers donde en la posicion 0 se encuentra la propiedad id
      // no es una buena practica almacenar la contraseña en la BD tal cual nos la manda el usuario, lo que se hace es hashear la password (encriptacion)
    } catch (error) {
      throw new Error(error.message);
    }
  }

  // creamos mutation para el login
  @Mutation(() => LoginResponse) // esta mutation nos va a devolver un objeto de tipo login response. No nos va a devolver usuario ni libro como antes
  async login(
    // nuestro argumento va a esperar un objeto de tipo LoginInput
    @Arg("input", () => LoginInput) input: LoginInput
  ) {
    try {
      // primero debemos comprobar si el email que nos pasa el usuario es un email que existe
      // desestructuramos el input del usuario
      const { email, password } = input;
      const userFound = await this.userRepository.findOne({ where: { email } });
      // en este caso, continuamos con la ejecucion del programa en caso no se encuentre el usuario

      if (!userFound) {
        const error = new Error();
        error.message = "Invalid credentials";
        throw error;
      }

      const isValidPassword: boolean = compareSync(
        password,
        userFound.password
      ); // lo que hace compareSync es hashear la contraseña ingresada por el usuario y compararla con la contraseña hasheada en BD (userFound.password)

      // ahora comprobar que la password sea correcta
      if (!isValidPassword) {
        const error = new Error();
        error.message = "Invalid credentials";
        throw error;
      }

      // vamos a generar un token con el id del usuario, y cuando decodeemos ese token nos va adevolver el id entonces vamos a poder usar el usuario logeado
      // ahora lo que haremos es firmar el token
      const jwt: string = sign({ id: userFound.id }, environment.JWT_SECRET); // como trabaja sincronicamente no necesitamos el await. Le vamos a pasar un payload (que es la informacion que vamos a codificar dentro del token), y una palabra secreta (por buenas practicas la palabra secreta debe estar dentro de las variables de entorno)

      return {
        // cuando el usuario se logea me devuelve el id y el token
        userId: userFound.id,
        jwt: jwt,
      };
    } catch (error) {
      throw new Error(error.message);
    }
  }
}
