// book, porque estamos trabajando con libros
// resolver, porque esta dentro de la carpeta resolvers
// aqui usamos libreria que nos va a facilitar escribir los esquemas en graphql

import { Mutation, Query, Resolver, Arg, InputType, Field } from "type-graphql"; // desestructuramos dos metodos, query y resolver
import { Author } from "../entity/author.entity";
import { getRepository, Repository } from "typeorm"; // para crear las consultas a BD
import { Length, IsString } from "class-validator";

// el decorador Arg va a hacer el trabajo por nosotros ...
// mediante un decorador, una consulta sencilla
// los decoradores son una propuesta para incluir en Javascript que nos va a permitir añadir anotaciones, metadatos, cambiar comportamiento de clases, metodos, parametros, etc., y que estos decoradores estan en una etapa experimental
// con esto evitamos escribir querys en graphql de la forma: (ya que lo hace por nosotros usando typescript)
/*
`
    Query {

    }

    Mutation {

    }

`
*/

// esta clase la podriamos crear en un archivo aparte pero como la app es pequeña no lo vamos a realizar
@InputType()
class AuthorInput {
  @Field() // igual que lo hicimos con las entidades especificamos que es un field
  @Length(3, 64)
  @IsString() // aunque es innecesario ya que al momento de compilar el typescript ya valida ello
  fullName!: string;
}

// creando input para el update
@InputType()
class AuthorUpdateInput {
  @Field(() => Number)
  @Length(3, 64)
  id!: number; // el operador ! indica que debe ser obligatorio el id

  @Field() // igual que lo hicimos con las entidades especificamos que es un field
  fullName?: string; // el operador ? indica que es opcional (no vamos a requerir actualizar todos los campos)
}

@InputType()
class AuthorIdInput {
  @Field(() => Number) // igual que lo hicimos con las entidades especificamos que es un field
  id!: number; // especificamos que es obligatorio que nos pasen el id como argunmento
}

// con lo hecho anteriormente ya podemos indicarle que esta mutacion va a devolver un Author con las mismas propiedades que su entidad
@Resolver()
export class AuthorResolver {
  // creamos el primer metodo parte de nuestras CRUD operations

  authorRepository: Repository<Author>; // al Repository le tenemos que pasar el Author

  constructor() {
    this.authorRepository = getRepository(Author); // al metodo get repository le pasamos de que entidad queremos. Aqui estamos inicializando el atributo. Asi tenemos habilitados todos los metodos para consulta en BD
  }

  // el metodo createAuthor va a ser una mutation
  // una mutation se encarga de crear o guardar datos en nuestra BD
  @Mutation(() => Author) // esa mutation nos va a devolver un objeto de tipo Author ya que vamos a querer que nos devuelva todos los datos del autor que vamos a crear. Pero al indicar Author, recuerda que el objeto de tipo Author refiere a una entidad, pero eso no tiene nada que ver con lo que es Graphql, por lo hay que hacer algunos cambios en la entity
  async createAuthor(
    @Arg("input", () => AuthorInput) input: AuthorInput
  ): Promise<Author | undefined> {
    // no debemos olvidarnos de manejar los errores con un try catch
    try {
      // especificamos el tipo de retorno (una promesa de tipo Author)
      const createdAuthor = await this.authorRepository.insert({
        fullName: input.fullName,
      }); // le decimos que espere porque es un metodo asincrono la consulta a BD. input.fullName trae la data del author input que creamos previamente. Podriamos haberpasado el input entero pero como es un solo campo es mas didactico para entender. ESTE METODO INSERT NOS VA A DEVOLVER UNA PROMESA DE TIPO InsertResult, que es un tipo de typeorm. Una de sus propiedades del InsertResult es identifier, que es la que nos da el ID de la entidad creada. asi que tenemos que acceder a eso
      const result = await this.authorRepository.findOne(
        createdAuthor.identifiers[0].id
      ); // de todos los metodos que hay el mejor es findOne. accedemos al primer elemento del array identifiers
      return result; // result es un Author
    } catch {
      console.error;
    }
  } // lo mejor es crear una clase, un objeto e indicar los campos que queremos crear. ESTO NOS VA A DEVOLVER UN OBJETO DE TIPO AUTHORINPUT. Y en el argumento de createAuthor indicamos que vamos a recibir un input de tipo authorInput

  // continuamos con nuestro CRUD. Creando metodo para la query de consulta
  // tenemos que indicarle mediante typegraphql con la notation query que es una query
  @Query(() => [Author]) // le ponemos entre corchetes porque debe devolver un array de Authors
  async getAllAuthors(): Promise<Author[]> {
    // colocamos el tipo de retorno del metodo
    // recuerda que findOne te devuelve un solo objeto, find, en cambio, te devuelve un array, incluso cuando el resultado es uno solo
    return await this.authorRepository.find({ relations: ["books"] }); // debemos considerar la relation ya que un autor puede tener muchos libros
  }

  // creamos un metodo para obtener el autor por ID (otra query)
  @Query(() => Author) // nos va a devolver un solo autor
  async getOneAuthor(
    @Arg("input", () => AuthorIdInput) input: AuthorIdInput // necesitamos pararle un argumento que contenga el id del autor a buscar. Nuestra variable se va a llamar input y va a ser de tipo AuthorIdInput (esto tambien lo hemos creado mas arriba en los inputs)
  ): Promise<Author | undefined> {
    try {
      // puede devolver undefined en caso de que no encuentre el autor
      const author = await this.authorRepository.findOne(input.id); // le pasamos el input.id para buscar al autor por id
      if (!author) {
        // arrojando un error que creamos en caso no exista el autor
        const error = new Error();
        error.message = "Author does not exists";
        throw error;
      }
      return author;
    } catch (e) {
      throw new Error(e);
    }
  }

  // creamos una mutation para hacer update de un autor
  @Mutation(() => Author) // hacemos que nos devuelva el autor que actualizamos
  async updateOneAuthor(
    // en este input debemos pasarle el id del autor que queremos actualizar y los campos a actualizar
    @Arg("input", () => AuthorUpdateInput) input: AuthorUpdateInput
  ): Promise<Author | undefined> {
    // podriamos utilizar el metodo this.authorRepository.update pero ese metodo no nos retorna el autor que es lo que queremos luego de hacer el update, nos retorna algo general
    // return await this.authorRepository.update(input.id, {fullName: input.fullName})

    // si queremos agregar una logica de negocio para que, en caso no exista el autor a actualizar, cree dicho autor
    const authorExists = await this.authorRepository.findOne(input.id);

    if (!authorExists) {
      throw new Error("Author does not exists"); // arroja error si el autor no existe
    }

    // guardamos el resultado de la ejecucion del metodo save para luego volver a hacer una consulta y devolver todos los campos del autor actualizado
    const updatedAuthor = await this.authorRepository.save({
      id: input.id,
      fullName: input.fullName,
    });

    // devolviento todos los campos del autor
    return await this.authorRepository.findOne(updatedAuthor.id);
  }

  // creando un mutation para eliminar un autor
  @Mutation(() => Boolean)
  async deleteOneAuthor(
    @Arg("input", () => AuthorIdInput) input: AuthorIdInput // reutlizamos el AuthorIdInput ya que eliminaremos un autor por su id
  ): Promise<Boolean> {
    try {
      // no necesitamos almacenar en una variable, usamos directamente await
      // await this.authorRepository.remove() // ESE METODO REMOVE permite eliminar varios elementos y retorna la cantidad de elementos eliminados, pero no queremos eso, solo queremos eliminar uno. En el remove se le pasa un array
      const author = await this.authorRepository.findOne(input.id);
      if (!author) throw new Error("Author does not exist"); // verificar que no se este eliminando un author que no existe
      await this.authorRepository.delete(input.id);
      return true;
    } catch (e) {
      throw new Error(e.message);
    }
  }
}
// lo anterior simplemente fue la forma en que se crea una consulta
