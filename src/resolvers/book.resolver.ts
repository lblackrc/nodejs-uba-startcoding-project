// book, porque estamos trabajando con libros
// resolver, porque esta dentro de la carpeta resolvers
// aqui usamos libreria que nos va a facilitar escribir los esquemas en graphql
// import { Query, Resolver } from "type-graphql"; // desestructuramos dos metodos, query y resolver
// alli estamos importando los decoradores query y resolver

// mediante un decorador, una consulta sencilla
// los decoradores son una propuesta para incluir en Javascript que nos va a permitir añadir anotaciones, metadatos, cambiar comportamiento de clases, metodos, parametros, etc., y que estos decoradores estan en una etapa experimental
// con esto evitamos escribir querys en graphql de la forma: (ya que lo hace por nosotros usando typescript)
/*
`
    Query {

    }

    Mutation {

    }

`
// */

// @Resolver()
// export class BookResolver {
//   @Query(() => String) // una query que por el momento nos va a devolver un String
//   getAll() {
//     return "All my books";
//   } // el metodo que vamos a llamar es getAll y nos devuelve el string indicado
// }
// // lo anterior simplemente fue la forma en que se crea una consulta

import {
  Mutation,
  Query,
  Resolver,
  Arg,
  InputType,
  Field,
  UseMiddleware,
  Ctx,
} from "type-graphql"; // desestructuramos dos metodos, query y resolver
import { getRepository, Repository } from "typeorm"; // para crear las consultas a BD
import { Author } from "../entity/author.entity";
import { Book } from "../entity/book.entity"; // importamos la entidad libro de nuestras entidades
import { Length } from "class-validator"; // esta libreria utiliza en el fondo validator.js. Por ejemplo nos sera util para evitar crear libros con titulos string vacios
import { IContext, isAuth } from "../middlewares/auth.middleware";

// creamos una clase para el input de crear libro
@InputType()
class BookInput {
  @Field()
  @Length(3, 64) // minimo y maximo de caracteres permitidos
  title!: string; // ! indica que el campo es obligatorio

  @Field()
  author!: number; // desde el front end nos van a mandar el id del author
}

// creamos un input para el update
@InputType()
class BookUpdateInput {
  @Field(() => String, { nullable: true }) // indicamos que va a devolver tipo string y va a ser anulable
  @Length(3, 64)
  title?: string; // ? indica que el campo no es obligatorio

  @Field(() => Number, { nullable: true })
  author?: number;
}

// creamos otro input complementario al update
@InputType()
class BookUpdateParsedInput {
  @Field(() => String, { nullable: true }) // indicamos que va a devolver tipo string y va a ser anulable
  @Length(3, 64)
  title?: string; // ? indica que el campo no es obligatorio

  @Field(() => Number, { nullable: true })
  author?: Author; // observa que cambia el tipo de author de number a Author
}

// input type para la query
@InputType()
class BookIdInput {
  @Field(() => Number)
  id!: number;
}

@Resolver()
export class BookResolver {
  bookRepository: Repository<Book>; // el repositorio va a ser de tipo libro
  authorRepository: Repository<Author>;

  // creamos el constructor
  constructor() {
    this.bookRepository = getRepository(Book);
    this.authorRepository = getRepository(Author);
  }

  // la primer consulta a BD va a ser la mutacion para crear un libro
  @Mutation(() => Book)
  @UseMiddleware(isAuth) // le decimos que este resolver debe estar autenticado
  async createBook(
    @Arg("input", () => BookInput) input: BookInput,
    @Ctx() context: IContext
  ) {
    try {
      // userRepository.get(payload.id) // con esto tendria el usuario, o en el MiddleWare ponerle la propiedad user
      console.log(context.payload); // el contexto que le he pasado me indica el usuario que se encuentre logeado
      // primero necesitamos buscar el autor por id que nos han mandado
      // luego de haber creado el authorRepository ya podemos consultar los autores
      const author: Author | undefined = await this.authorRepository.findOne(
        input.author
      ); // aqui nos van a mandar el id del author
      // como author puede ser undefined, hay que manejar el error
      if (!author) {
        const error = new Error();
        error.message =
          "The author for this book does not exists, please double check";
        throw error; // terminamos la ejecucion del programa con el error
      }

      // ahora necesitamos crear el libro porque ya tenemos todos los campos necesarios
      const book = await this.bookRepository.insert({
        title: input.title,
        author: author, // ese autor es el que acabamos de buscar en nuestro repository. Ya sabemos que existe de lo contrario hubiera entrado en el if anterior
      });

      return await this.bookRepository.findOne(book.identifiers[0].id, {
        relations: ["author", "author.books"], // el "author.books hara que nos devuelva tambien los libros del author"
      }); // una de las tres propiedades que nos devuelve el insert result es identifier, el cual es un array en cuyo primer lugar esta el id del autor, del libro (del registro creado)
      // NO DEBEMOS OLVIDAR ESPECIFICAR LA RELACION, usamos "relations", el cual nos acepta un array de strings con las entidades que queremos relacionar (en nuestro caso, author)
    } catch (e) {
      throw new Error(e.message);
    }
  }

  @Query(() => [Book])
  @UseMiddleware(isAuth) // antes de entrar a este metodo va a chequear que el usuario este autenticado, que el usuario tenga un token y que ese token sea valido
  async getAllBooks(): Promise<Book[]> {
    // si hubiera filtros o sort lo pasamos como parametro de la funcion getAllBooks
    try {
      return await this.bookRepository.find({
        relations: ["author", "author.books"],
      }); // especificamos la relacion con author
    } catch (e) {
      throw new Error(e);
    }
  }

  // otra query mas
  @Query(() => Book) // nos retorna un solo libro
  async getBookById(
    @Arg("input", () => BookIdInput) input: BookIdInput
  ): Promise<Book | undefined> {
    try {
      const book = await this.bookRepository.findOne(input.id, {
        relations: ["author"],
      });
      // si no nos da el resultado de buscar el book
      if (!book) {
        const error = new Error();
        error.message = "Book not found";
        throw error;
      }
      return book; // si no entra en el condicional entonces se retorna el libro encontrado
    } catch (e) {
      throw new Error(e);
    }
  }

  @Mutation(() => Boolean)
  async updateBookById(
    @Arg("bookId", () => BookIdInput) bookId: BookIdInput, // necesitamos el id para actualizar el libro
    @Arg("input", () => BookUpdateInput) input: BookUpdateInput // otro argumento que le vamos a llamar input
  ): Promise<Boolean> {
    try {
      // como nos esta devolviendo Boolean la mutacion lo hacemos directamente
      await this.bookRepository.update(bookId.id, await this.parseInput(input));
      return true;
    } catch (e) {
      throw new Error(e);
    }
  }

  // nos falta una mutation para eliminar un libro
  @Mutation(() => Boolean)
  async deleteBook(
    @Arg("bookId", () => BookIdInput) bookId: BookIdInput
  ): Promise<Boolean> {
    try {
      const result = await this.bookRepository.delete(bookId.id); // para encontrar el libro y eliminarlo
      // result me devuelve un objeto con dos propiedades, una de ellas es la cantidad de tablas que fueron afectadas
      // result devuelve: DeleteResult {raw: [], affected: 0}
      if (result.affected === 0) throw new Error("Book does not exist"); // comprobar que no estemos eliminando un libro que no existe
      return true;
    } catch (e) {
      throw new Error(e);
    }
  }

  // creamos un metodo privado para esta clase
  // esta funcion nos ayudara a parsear la informacion que nos pasan (input) a la informacion que queremos almacenar en BD
  private async parseInput(input: BookUpdateInput) {
    try {
      const _input: BookUpdateParsedInput = {};
      // cada uno de los inputs que tenemos nos vienen en este input
      if (input.title) {
        _input["title"] = input.title; // la key del objeto que se llama title
      }

      if (input.author) {
        const author = await this.authorRepository.findOne(input.author);
        if (!author) {
          throw new Error("This author does not exist"); // si no encuentra el autor que termine la ejecucion y arroje el error
        }
        _input["author"] = await this.authorRepository.findOne(input.author); // aqui no necesitamos indicar la relacion, podriamos crear en la entidad book agregar en la propiedad author que se llame higher?, pero no necesitamos que haga eso la BD
      }

      return _input;
    } catch (e) {
      throw new Error(e);
    }
  }
}
