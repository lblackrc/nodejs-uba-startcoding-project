import express from "express"; // importamos el framework para crear el server
import "reflect-metadata"; // esta libreria es importante ya que de esta dependen typeorm y typegraphql
import { ApolloServer } from "apollo-server-express"; // con apollo server vamos a poder crear una ruta que nos sirva a la API de Graphql
import { buildSchema } from "type-graphql"; // buildschema transforma el typescript escrito en el resolver a lo que es el lenguaje tradicional de graphql (querys y resolvers)
// a apollo le debemos pasar nuestros resolvers
import { BookResolver } from "./resolvers/book.resolver";
import { AuthorResolver } from "./resolvers/author.resolver"; // no debemos olvidarnos de pasar los resolvers al server.ts
import { AuthResolver } from "./resolvers/auth.resolver";

// creamos una ruta para graphql ya que la usaremos para nuestras querys
export async function startServer() {
  const app = express(); // con esto incializamos uns servidor http
  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [BookResolver, AuthorResolver, AuthResolver],
    }),
    context: ({ req, res }) => ({ req, res }), // es todo lo que vamos a pasar a nuestra app y estara disponible para todos los resolvers. Le pasamos objetos de express (req y response). De alli vamos a poder obtener los headers, y dentro del request, obtenemos el header de actualizacion
  }); // nos muestra error porque debemos de pasar de graph a algo que pueda entender el servidor de Apollo
  // llamamos al metodo asincrono de buildSchema para pasar typescript a graphql, y le pasamos todos los resolvers que necesitemos
  // creamos un nuevo objeto de tipo ApolloServer y le pasamos nuestros resolver con schema
  // mediante un middleware le tenemos que decir que nuestro Apollo va a ejecutarse dentro de nuestro servidor express y en el mismo middleware le vamos a pasar la ruta
  apolloServer.applyMiddleware({ app, path: "/graphql" }); // app es el servidor http que creamos anteriormente
  return app;
}
