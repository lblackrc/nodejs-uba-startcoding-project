import dotenv from "dotenv"; // no necesitamos los tipos de datos para poder importarlo de esta manera ya que la libreria viene con esos tipos de datos agregados

dotenv.config(); // con esto se habilita el reconocimiento de parametros que defines abajo

export const environment = {
  PORT: process.env.PORT,
  DB_HOST: process.env.DB_HOST,
  DB_PORT: process.env.DB_PORT,
  DB_USERNAME: process.env.DB_USERNAME,
  DB_PASSWORD: process.env.DB_PASSWORD,
  DB_DATABASE: process.env.DB_DATABASE,
  JWT_SECRET: process.env.JWT_SECRET || "Default", // si el JWT_SECRET llega como undefined, entonces utiliza un string vacio (para evitar el undefined)
};
