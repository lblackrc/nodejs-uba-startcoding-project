import { createConnection } from "typeorm"; // con esto voy a poder indicar a que BD me voy a conectar pasando unos parametros
import path from "path"; // esto nos va a permitir subir un nivel usando dir (util para especificar una ruta exacta)
// importamos las variables de entorno
import { environment } from "./environment";
console.log(environment);

export async function connect() {
  await createConnection({
    type: "postgres", // la ventaja de typeorm es que con este simple parametro puedes cambiar el gestor de BD a utilizar
    port: Number(environment.DB_PORT), // puerto por default de postgres
    username: environment.DB_USERNAME,
    password: environment.DB_PASSWORD,
    database: environment.DB_DATABASE,
    entities: [path.join(__dirname, "../entity/**/**.ts")], // listado donde van a estar las entidades
    // estamos usando .ts porque el ts-node va a leer directamente los archivos de Typescript
    synchronize: true,
  }); // al create connection le pasamos un objeto de configuracion
  console.log("Database running");
}
