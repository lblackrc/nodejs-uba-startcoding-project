import { startServer } from "./server";
import { connect } from "./config/typeorm"; // importamos connect para acceder a la BD
import { environment } from "./config/environment"; // importamos para asignar el puerto al servidor

async function main() {
  connect(); // conexion a BD
  const port: number = Number(environment.PORT); // esto deberia ir en las variables de entorno, pero por ahora lo hacemos en la app
  const app = await startServer();
  app.listen(port);
  console.log(`App running on port ${port}`);
}

main(); // con esto tenemos basicamente nuestro servidor configurado
