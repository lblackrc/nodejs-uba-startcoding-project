// vamos a trabajar con typeorm
import { timeStamp } from "console";
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  ManyToOne,
} from "typeorm";
import { Author } from "./author.entity";
import { Field, ObjectType } from "type-graphql"; // decoradores nuevos para solucionar el problema de los resolvers (graphql)

// typeorm maneja la estructura de nuestras tablas mediante clases de typescript
// mediante decoradores podemos especificar que tipo va a ser cada columna
// siempre necesitamos que cada tabla tengo una PK (primary column): importamos PrimaryColumn
// queremos ademas que el id sea autoincrementable
// para que la clase Book sea interpretada como una Entity, debe extender de Typeorm

@ObjectType()
@Entity() // usamos el decorador Entity para que Book herede de Entity
export class Book {
  @Field()
  @PrimaryGeneratedColumn() // crea una PK autoincrementable
  id!: number; // el ! indica que siempre lo vamos a inicializar

  @Field()
  @Column()
  title!: string;

  @Field(() => Author) // no le indicamos que es nullable porque cuando creemos un libro ya vamos a tener el autor. No debemos olvidar de indicar que esto va a retornar un objeto de tipo Author
  @ManyToOne(() => Author, (author) => author.books, { onDelete: "CASCADE" }) // creamos la relacion de muchos a uno. eL onDelete CASCADE me permitira borrar todos los libros del autor que haya sido eliminado. Todos los libros que esten relacionados a ese autor
  author!: Author;

  @Field()
  @CreateDateColumn({ type: "timestamp" }) // a este decorador le pasamos de que tipo va a ser la fecha
  createdAt!: string; // si bien es de tipo fecha le ponemos string
}
