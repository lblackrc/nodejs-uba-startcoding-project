// vamos a trabajar con typeorm
// CREAMOS ESTA ENTIDAD PARA EL MANEJO DE AUTENTICACION CON USUARIOS
import { timeStamp } from "console";
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  ManyToOne,
} from "typeorm";
import { Author } from "./author.entity";
import { Field, ObjectType } from "type-graphql"; // decoradores nuevos para solucionar el problema de los resolvers (graphql)

@ObjectType()
@Entity() // usamos el decorador Entity para que Book herede de Entity
export class User {
  @Field()
  @PrimaryGeneratedColumn() // crea una PK autoincrementable
  id!: number; // el ! indica que siempre lo vamos a inicializar

  @Field()
  @Column()
  fullName!: string;

  @Field()
  @Column()
  email!: string;

  @Field()
  @Column()
  password!: string;

  @Field()
  @CreateDateColumn({ type: "timestamp" }) // a este decorador le pasamos de que tipo va a ser la fecha
  createdAt!: string; // si bien es de tipo fecha le ponemos string
}
