// vamos a trabajar con typeorm
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  OneToMany,
} from "typeorm";

import { Book } from "./book.entity";
import { Field, ObjectType } from "type-graphql"; // decoradores nuevos para solucionar el problema de los resolvers (graphql)

// typeorm maneja la estructura de nuestras tablas mediante clases de typescript
// mediante decoradores podemos especificar que tipo va a ser cada columna
// siempre necesitamos que cada tabla tengo una PK (primary column): importamos PrimaryColumn
// queremos ademas que el id sea autoincrementable
// para que la clase Book sea interpretada como una Entity, debe extender de Typeorm

// DEBEMOS INDICAR QUE LOS CAMPOS QUE NOS DEVUELVE ESTA CLASE VAN A SER LOS CAMPOS QUE NOS VA A DEVOLVER GRAPHQL
// indicamos que esta clase sera de tipo Object Type
// CADA UNO DE LOS CAMPOS DE LA CLASE AHORA LO MARCAMOS CON EL DECORADOR FIELD (decimos que es un campo)
@ObjectType() // cuando queremos retornar algo que no es un numero ni un string a una API, sino que queremos retornar un objeto que es algo mas complejo como en este caso, podemos especificarlo agregando este decorador
@Entity() // usamos el decorador Entity para que Book herede de Entity
export class Author {
  @Field()
  @PrimaryGeneratedColumn() // crea una PK autoincrementable
  id!: number; // el ! indica que siempre lo vamos a inicializar

  @Field(() => String) // podemos indicar que va a retornar un string, aunque ya no es necesario porque con typescript ya lo indicas en la linea de abajo
  @Column()
  fullName!: string;

  @Field(() => [Book], { nullable: true }) // cuando creemos un autor no necesariamente vamos a tener libros asignados, por eso marcamos como nullable
  @OneToMany(() => Book, (book) => book.author, { nullable: true }) // one to many porque un Author puede tener muchos Books escritos
  books!: Book[]; // un array de libros

  @Field(() => String)
  @CreateDateColumn({ type: "timestamp" }) // a este decorador le pasamos de que tipo va a ser la fecha
  createdAt!: string; // si bien es de tipo fecha le ponemos string
}

// primero tendremos que crear un author y luego a ese author le asignamos un libro cuando se crea
